package com.cgi.osd.bestpractice;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class NullReturn {

    public String returnInput(String input) {
	return input;
    }

    public Collection<String> returnEmptyIfNull(String input) {

	if (input == null) {
	    return Collections.emptyList();
	}
	final List<String> result = new LinkedList<>();
	result.add(input);
	return result;
    }

    public Optional<String> returnOptional(String input) {
	if (input == null) {
	    return Optional.empty();
	}
	return Optional.of(input);
    }

    private void sonarDummy() {
	final Optional<String> stupidAssignment = null;
    }

}
