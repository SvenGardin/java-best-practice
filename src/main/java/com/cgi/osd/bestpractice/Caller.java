package com.cgi.osd.bestpractice;

import java.util.Collection;

public class Caller {

    final NullReturn nullReturn = new NullReturn();

    public void demoReturnHandling() {
	final String[] inputs = { "Hej hopp!", null };

	for (final String input : inputs) {
	    System.out.println("\nFunction parameter is now: " + input + "\n");
	    invokeMethods(input);
	}
    }

    private void invokeMethods(String input) {
	try {
	    final String result = this.nullReturn.returnInput(input);
	    System.out.println("returnInput result was: " + result + " and lenght was: " + result.length());
	} catch (final NullPointerException e) {
	    System.out.println("Got exception! " + e);
	}

	final Collection<String> collection = this.nullReturn.returnEmptyIfNull(input);
	if (!collection.isEmpty()) {
	    System.out.println("returnEmptyIfNull result was: " + collection.iterator().next());
	}
	System.out.println("Collection lenght was: " + collection.size());

	final String optionalResult = this.nullReturn.returnOptional(input).orElse("Result empty");
	System.out.println("returnOptional result was: " + optionalResult);

    }
}
