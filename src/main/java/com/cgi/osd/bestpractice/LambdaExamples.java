package com.cgi.osd.bestpractice;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class LambdaExamples {

    List<Person> people = new LinkedList<>();

    public void sortJava8() {
	Collections.sort(people, new Comparator<Person>() {
	    @Override
	    public int compare(Person p1, Person p2) {
		return p1.getName().compareTo(p2.getName());
	    }
	});
    }

    public void sortJava9() {
	people.sort((p1, p2) -> p1.getName().compareTo(p2.getName()));
    }

    public void sortAddresses() {
	people.sort(LambdaExamples::compareAddress);
    }

    private static Integer compareAddress(Person p1, Person p2) {
	try {
	    return p1.getAddress().compareTo(p2.getAddress());
	} catch (final Exception e) {
	    throw new RuntimeException();
	}
    }
}